console.log ("Hello World");

// ARITHMETIC OPERATORS
let x = 1357;
let y = 7821;

// Addition Operator
let sum = x + y;
console.log("Result of Addition Operator: " + sum);

// Subtraction Operator
let difference = y - x;
console.log("Result of Subtraction Operator: " + difference);

// Multiplication Operator
let product = x * y;
console.log("Result of Multiplication Operator: " + product);

// Division Operator
let quotient = y / x;
console.log("Result of Division Operator " + quotient);

// Modulo Operator
let remainder = y % x;
console.log("Result of Modulo operator: " + remainder);

// ASSIGNMENT OPERATOR

// Basic Assignment Operator (=)
// Assignment operator adds the value of the right operand to a variable and assigns the result to the variable

let assignmentNumber = 8;
console.log(assignmentNumber);

// Addition Assignment Operator
// Shorthand -- 
// assignmentNumber = assignmentNumber + 2;
assignmentNumber += 2;
console.log(assignmentNumber); //10


// Subtraction/Multiplication/Division Assignment Operator (-=, *=, /=);

assignmentNumber -=2; //assignmentNumber = assignmentNumber - 2;
console.log(assignmentNumber); //8

assignmentNumber *= 2;
console.log(assignmentNumber); //16

assignmentNumber /= 2;
console.log(assignmentNumber); //8

// Multiple Operators and Parentheses
/*
	When multiple operators are applied in a single statement, it follows PEMDAS 
*/

let mdas = 1 + 2 - 3 * 4 / 5;
console.log("Result of the mdas operation: " + mdas);

let pemdas = 1 + (2 - 3) * (4 / 5);
console.log("Result of the pemdas operation: " + pemdas);

// Exponent
let totalPemdas = 5**2 + (10 - 2) / 2 * 3;
console.log("Result of the exponent operation: " + totalPemdas);

// Increment and Decrement
// Operators that allow us to add or subtract values by 1 and reassigns value of variable where increment/decrement was applied to.

let z = 1;

// Value of z is added by value of 1 before returning the value and storing it in the variable "increment"
//Pre-Increment
let increment = ++z;
console.log("Result of pre-increment: "+increment); //2
console.log(z); //2

// Value of z is returned and stored in the variable "increment " then value of z is increased by 1 
// Post-Increment
increment = z++;
console.log("Result of post-increment: "+increment);//2
console.log(z);//3

// Pre-Decrement
let decrement = --z;
console.log("Result of pre-decrement: "+decrement);//2
console.log(z);//2

decrement = z--;
console.log("Result of post-decrement: "+decrement);//2
console.log(z);//1

// Type Coercion
/*
	- Type coercion is automatic or implicit conversion values from one data type to another.

	- Happens when operations are performed on different data types that would normally not be possible.
*/

let numA = '10';
let numB = 12;

/*
	- Adding/concatenating string and number will result string
*/

let coercion = numA + numB;
console.log(coercion);
console.log(typeof coercion);

// Non-Coercion
let numC = 16;
let numD = 14;

let nonCoercion = numC + numD;
console.log(nonCoercion);
console.log(typeof nonCoercion);

// Adding Boolean and Number
/*
	Result is a number
	Boolean "true" is associated with value 1
	Boolean "false" is associated with value 0
*/

let numE = true + 1;
console.log(numE);//2

let numF = false + 1;
console.log(numF);//1

// Comparison Operator
let juan = "juan";

// Equality Operator (==);
/*
	Checks whether operands are equal/have same content
	Attemps to compare operands of different data types
	Return boolean value
*/

console.log(1 == 1);//true
console.log(1 == 2);//false
console.log(1 == "1");//true b/c same content
console.log(0 == false);//true
//Compares two strings that are the same 
console.log('juan' == 'juan');//true
// Compares string with variable "juan" declared above
console.log('juan' == juan); //true

//Inequality Operator(!=)
/*
	Checks whether operant are not equal/have different content
*/ 

console.log(1 != 1);//false
console.log(1 != 2);//true
console.log(1 != '1')//false
console.log(0 != false);//false
console.log('juan' != 'juan');//false
console.log('juan' != juan);//false

// Strict Equality Operator (===)
/*
	- Checks whether operands are equal or have same content
	- Also COMPARES data types of two values

*/ 

console.log(1 === 1);//true
console.log(1 === 2);//false
console.log(1 === '1')//false
console.log(0 === false);//false
console.log('juan' === 'juan');//true
console.log('juan' === juan);//true

// Strict Inequality Operator
/*
	- Checks whether operands are not equal/have same content
	- Also COMPARES data types of two values
*/

console.log(1 !== 1);//false
console.log(1 !== 2);//true
console.log(1 !== '1')//true
console.log(0 !== false);//true
console.log('juan' !== 'juan');//false
console.log('juan' !== juan);//false

// Relational Operators

let a = 50;
let b = 65;

//GT or greater than operator (>) 
let isGreaterThan = a > b;
console.log(isGreaterThan);//false 
// LT or less than operator (<)
let isLessThan = a < b;
console.log(isLessThan);//true 
//Greater than or equal operator (>=)
let isGTorEqual = a >= b;
console.log(isGTorEqual);//false
// Less than or equal operator (<=)
let isLTorEqual = a <= b;
console.log(isLTorEqual);//true

let numStr = "30";
console.log(a > numStr);//true

// conversion into a number is unsuccessful (NaN) b/c it is a text string
let str = "twenty";
console.log(b >= str);//NaN -> false

// Logical Operator

let isLegalAge = true;
let isRegistered = false;

// Logical AND Operator (&& - Double Ampersand)
// true && true = true
// true && false = false

let allRequirementsMet = isLegalAge && isRegistered;
console.log(allRequirementsMet);//false

// Logical OR Operator (|| - Double Pipe)
// Returns true if one of operands is true
// true || true = true
// true || false = true
// false|| false = false
let someRequirementsMet = isLegalAge || isRegistered;
console.log(someRequirementsMet);

//Logical NOT Operator (! - Exclamation Point)
// Returns opposite value
let someRequirementsNotMet = !isRegistered;
console.log(someRequirementsNotMet);//true

